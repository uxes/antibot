const TelegramBot = require('node-telegram-bot-api');
const token = ''; //todo: fill me
const bot = new TelegramBot(token, {polling: true});
let messageHistory = [];

bot.on('message', (msg) => {
  const chatId = msg.chat.id;
  const messageId = msg.message_id
  const userId = msg.from.id

  messageHistory.push({ chatId, messageId, userId })

  if(/.*bit.ly.*/.test(msg.text)) {
    messageHistory = messageHistory.filter( m => {
      if(m.userId === msg.from.id) {
        bot.deleteMessage(m.chatId, m.messageId)
        return false
      }
      return true
    })
  }
});
